#!/usr/bin/env sh

set -ex
. ./env.sh

mkdir -p build

build_ctr=$(buildah from -v /dev/null:/centos_root/dev/null "$CI_REGISTRY_IMAGE"/toolbox:"$CI_COMMIT_BRANCH")

arch=$1
if [ -z "$arch" ]; then
  arch=$(buildah inspect --format "{{.OCIv1.Architecture}}" "$build_ctr")
fi

case $arch in
  amd64)
    rpm_arch=x86_64
    ;;
  arm64)
    rpm_arch=aarch64
    ;;
  *)
    >&2 echo "Unknown architecture: $arch"
    exit 1
    ;;
esac

buildah run "$build_ctr" -- mkdir -p /centos_root
buildah config --workingdir /centos_root "$build_ctr"

buildah run "$build_ctr" -- rpm --root /centos_root --import https://www.centos.org/keys/RPM-GPG-KEY-CentOS-Official-SHA256
buildah run "$build_ctr" -- dnf -y --installroot=/centos_root \
  --repofrompath=centos10,https://mirror.stream.centos.org/10-stream/BaseOS/\$basearch/os/ \
  --repo=centos10 \
  --forcearch=$rpm_arch \
  install centos-stream-repos
buildah run "$build_ctr" sh -c "echo 'LANG=C.UTF-8' > etc/locale.conf"
buildah run "$build_ctr" -- mkdir -p etc/rpm
buildah run "$build_ctr" sh -c "echo \"%_install_langs en_US\" > etc/rpm/macros.image-language-conf"
buildah add "$build_ctr" dnf.conf etc/dnf/
buildah run "$build_ctr" sh -c "echo 'container' > etc/dnf/vars/infra"

buildah run "$build_ctr" -- dnf -y --installroot=/centos_root --forcearch=$rpm_arch install \
  ca-certificates \
  coreutils-single \
  glibc-minimal-langpack \
  libgcc

buildah run "$build_ctr" -- sh -c "cp -r etc/skel/. root/"

buildah run "$build_ctr" -- dnf --installroot=/centos_root clean all
buildah run "$build_ctr" sh -c "rm var/lib/dnf/history.*"

buildah run "$build_ctr" -- tar -cz --exclude dev/null . > "build/centos_$arch.tar.gz"

buildah rm "$build_ctr"

ctr=$(buildah from scratch)

buildah add "$ctr" "build/centos_$arch.tar.gz" /

buildah config --arch "$arch" "$ctr"
buildah config --label "name=base" "$ctr"
buildah config --label "commit=$CI_COMMIT_SHA" "$ctr"

ctr=$(buildah from --arch "$arch" "$(buildah commit --rm "$ctr" base)")
mnt=$(buildah mount "$ctr")
build_ctr=$(buildah from -v "$mnt":/centos_root "$CI_REGISTRY_IMAGE"/toolbox:"$CI_COMMIT_BRANCH")

buildah run "$build_ctr" -- dnf -y  --installroot=/centos_root --forcearch=$rpm_arch install \
  dnf \
  findutils \
  gzip \
  rsync \
  tar \
  wget \
  xz

buildah run "$build_ctr" -- dnf --installroot=/centos_root clean all

buildah config --label "name=toolbox" "$ctr"
buildah config --label "commit=$CI_COMMIT_SHA" "$ctr"

buildah commit --rm "$ctr" toolbox
buildah rm "$build_ctr"

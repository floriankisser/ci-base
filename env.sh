#!/usr/bin/env sh

if [ -z "$CI" ]; then
  CI_REGISTRY=registry.gitlab.com
  CI_PROJECT_ROOT_NAMESPACE=floriankisser
  CI_REGISTRY_IMAGE=$CI_REGISTRY/$CI_PROJECT_ROOT_NAMESPACE/ci-base
  CI_COMMIT_BRANCH=$(git rev-parse --abbrev-ref HEAD)
  CI_COMMIT_SHA=$(git rev-parse HEAD)
fi

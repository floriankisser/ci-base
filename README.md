# CI Base Images

## Images

### base

Minimal environment with glibc, coreutils and ca-certificates.

### toolbox

Useful tools, especially dnf to extend base.

## Build locally

Run as unprivileged user, produces `localhost/base` and `localhost/toolbox`.

```sh
buildah unshare ./build.sh
```
